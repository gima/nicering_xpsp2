:: changes service state via sc.exe and informs of success or failure

@echo off

if "%1" == "" goto :usage
if "%2" == "" goto :usage

sc config %1 start= %2 | grep -P "FAILED|SYNTAX" > nul

if %errorlevel%==0 (
 echo Failed to change service "%1" state to "%2"
) else (
 echo Changed service "%1" state to "%2"
)

goto :eof

:usage
echo Usage: %0 [service name] [boot/system/auto/demand/disabled]
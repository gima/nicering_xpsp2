:: this batch is to be ran, it does things right order

@echo off
setlocal
cls

set patching_dir=%cd%

echo - Disabling "Windows File Protection" until next reboot
util_disablewfptemporarily.exe
echo.
echo.


echo - Disabling "Windows File Protection" permanently
call :patchfile "%systemroot%\system32" winlogon.exe patchfile_disablewfp.txt
echo.
echo.


echo - Removing simultaneous connection limit
call :patchfile "%systemroot%\system32\drivers" tcpip.sys patchfile_disableconnectionlimit.txt
echo.
echo.


echo - Removing creation of post boot reminders
call :patchfile "%systemroot%" explorer.exe patchfile_disablepostbootreminders.txt
echo.
echo.


echo - Applying "HKEY_CURRENT_USER" registry patches
reg import reg_currentuser.reg > nul
echo.
echo.

echo - Applying other registry patches
reg import reg_other.reg > nul
echo.
echo.


echo - Modifying service start states
:: disables those annoying beeps that you hear from pc speaker
call batch_sc beep disabled

:: these three (with .reg file) disable ports that the operating system is listening
call batch_sc alg disabled
call batch_sc ssdpsrv disabled
call batch_sc w32time disabled

call batch_sc cryptsvc demand
call batch_sc dmadmin demand
call batch_sc wuauserv disabled
call batch_sc dnscache disabled
call batch_sc ersvc disabled
call batch_sc helpsvc disabled
call batch_sc irmon disabled
call batch_sc protectedstorage disabled
call batch_sc remoteregistry disabled
call batch_sc wscsvc disabled
call batch_sc shellhwdetection disabled
call batch_sc schedule disabled
call batch_sc spooler disabled
call batch_sc lmhosts disabled
call batch_sc netman auto
call batch_sc nla auto
call batch_sc themes disabled
call batch_sc sharedaccess disabled
call batch_sc fastuserswitchingcompatibility disabled
call batch_sc termservice disabled
call batch_sc policyagent disabled
call batch_sc clipsrv disabled
call batch_sc hidserv disabled
call batch_sc netdde disabled
call batch_sc netddedsdm disabled
call batch_sc remoteaccess disabled
call batch_sc tlntsvr disabled
call batch_sc messenger disabled
call batch_sc msdtc disabled
call batch_sc sysmonlog disabled
call batch_sc httpfilter disabled
call batch_sc imapiservice disabled
call batch_sc cisvc disabled
call batch_sc swprv disabled
call batch_sc mnmsrvc disabled
call batch_sc xmlprov disabled
call batch_sc wmdmpmsn disabled
call batch_sc rsvp disabled
call batch_sc rasauto disabled
call batch_sc rasman disabled
call batch_sc rdsessmgr disabled
call batch_sc rpclocator disabled
call batch_sc ntmssvc disabled
call batch_sc scardsvr disabled
call batch_sc tapisrv disabled
call batch_sc trkwks disabled
call batch_sc upnphost disabled
call batch_sc webclient disabled
call batch_sc wmiapsrv disabled
call batch_sc vss disabled
rem sc delete winmgmt
rem sc delete wmi
echo.
echo.


echo - Turning hibernation off
powercfg /hibernate off
echo.
echo.


echo - Removing scripting engine
call :eradicatefile wscript.exe
call :eradicatefile cscript.exe
echo.
echo.


echo - Removing shs crap
call :eradicatefile shscrap.dll
echo.
echo.


echo - Removing "Windows Media Player" from context menus
call :unregister shmedia.dll
call :eradicatefile shmedia.dll
echo.
echo.


echo - Removing explorer integrated .zip viewer and extractor
call :unregister zipfldr.dll
call :eradicatefile zipfldr.dll
echo.
echo.


echo - Removing explorer integrated .cab viewer and extractor
call :unregister cabview.dll
call :eradicatefile cabview.dll
echo.
echo.


echo - Removing "Picture and fax viewer"
call :unregister shimgvw.dll
call :eradicatefile shimgvw.dll
echo.
echo.


echo - Emptying prefetch folder
call :emptyfolder "%systemroot%\prefetch"
echo.
echo.


echo - Emptying sendto folder
call :emptyfolder "%userprofile%\sendto"
echo.
echo.


echo - Emptying all users start menu root
pushd "%allusersprofile%\k�ynnist�-valikko"
del /f /q * > nul
popd
:: call :emptyfolder 
echo.
echo.


echo - Setting numlock on, capslock and scroll-lock off
util_keyserstate 1 0 0
echo.
echo.


echo.
echo.
echo Batch file finished. Remember to uninstall everything but
echo "Tcp/Ip protocol" from every network adapter and also
echo to disable lmhosts lookup and netbios (from tcp/ip
echo advanced settings)
echo.
echo.
echo Press any key to reboot
pause > nul

endlocal
shutdown -r -t 00

goto :eof



:: renames %2 to %2.bak, copies %2.bak to %2 and patches %2

:patchfile
pushd %1

call :ren %1\ %2 %2.bak
copy /y %2.bak %2 > nul
"%patching_dir%\util_filepatcher" "%patching_dir%\%3" %2

popd
goto :eof



:: renames %1 to %1.bak and if %1.bak exists before this, deletes it

:ren
if exist %1%3 (
 del /f %1%3
)
if exist %1%2 (
 ren %1%2 %3
)
goto :eof



:: unregisters a file and informs of success or failure

:unregister

regsvr32 /u /s %1
if %errorlevel% == 0 ( echo.>nul ) else (
 echo Failed to unregister "%1"
)

goto :eof



:: uses del command to find and delete all files from drive where %systemroot% is

:eradicatefile
pushd %systemroot%
cd\
for /f %%i in ('dir "%1" /b /s') do call :ren %%~di%%~pi %%~ni%%~xi %%~ni%%~xi.bak
:: 
:: del /f /s "%1"
:: 
popd
goto :eof



:: empties a folder, using pushd and popd because if not viewing hidden files, del can't do it

:emptyfolder

pushd %1
del /f /s /q * > nul
popd